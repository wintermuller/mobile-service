package com.example.mobileservice;

/**
 *
 * @author vrg
 */
public class Mobile {
    public Manufacturer manufacturer;
    public String model;
    public MobilePart display;
    public MobilePart motherBoard;
    public MobilePart keyboard;
    public MobilePart microphone;
    public MobilePart speaker;
    public MobilePart volumeButtons;
    public MobilePart powerSwitch;
    public String otherInfo;

    @Override
    public String toString() {
        return manufacturer + " " + model + ", other Info: " + otherInfo;
    }
    
    
}
