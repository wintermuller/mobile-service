package com.example.mobileservice;

/**
 *
 * @author vrg
 */
public class ReplacedMobilePart extends MobilePart {

    public ReplacedMobilePart(MobilePartType type, String name) {
        super(type, false, name);
    }
    
}
